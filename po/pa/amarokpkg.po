# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# A S Alam <aalam@users.sf.net>, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-04 01:45+0000\n"
"PO-Revision-Date: 2010-09-26 09:08+0530\n"
"Last-Translator: A S Alam <aalam@users.sf.net>\n"
"Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "aalam@users.sf.net"

#: amarokpkg.cpp:87 amarokpkg.cpp:99
#, kde-format
msgid "Amarok Applet Manager"
msgstr "ਅਮਰੋਕ ਐਪਲਿਟ ਮੈਨੇਜਰ"

#: amarokpkg.cpp:89
#, kde-format
msgid "(C) 2008, Aaron Seigo, (C) 2009, Leo Franchi"
msgstr "(C) 2008, Aaron Seigo, (C) 2009, Leo Franchi"

#: amarokpkg.cpp:90
#, kde-format
msgid "Aaron Seigo"
msgstr "Aaron Seigo"

#: amarokpkg.cpp:91
#, kde-format
msgid "Original author"
msgstr "ਅਸਲੀ ਲੇਖਕ"

#: amarokpkg.cpp:93
#, kde-format
msgid "Leo Franchi"
msgstr "Leo Franchi"

#: amarokpkg.cpp:94
#, kde-format
msgid "Developer"
msgstr "ਡਿਵੈਲਪਰ"

#: amarokpkg.cpp:110
#, kde-format
msgid "For install or remove, operates on applets installed for all users."
msgstr "ਸਭ ਯੂਜ਼ਰਾਂ ਲਈ ਐਪਲਿਟ ਉੱਤੇ ਇੰਸਟਾਲ, ਹਟਾਉਣ, ਕਾਰਵਾਈਆਂ"

#: amarokpkg.cpp:112
#, kde-format
msgctxt "Do not translate <path>"
msgid "Install the applet at <path>"
msgstr "<path> ਉੱਤੇ ਐਪਲਿਟ ਇੰਸਟਾਲ"

#: amarokpkg.cpp:114
#, kde-format
msgctxt "Do not translate <path>"
msgid "Upgrade the applet at <path>"
msgstr "<path> ਉੱਤੇ ਐਪਲਿਟ ਅੱਪਗਰੇਡ"

#: amarokpkg.cpp:116
#, fuzzy, kde-format
#| msgid "List installed applets"
msgid "Most installed applets"
msgstr "ਇੰਸਟਾਲ ਐਪਲਿਟ ਦੀ ਲਿਸਟ"

#: amarokpkg.cpp:118
#, kde-format
msgctxt "Do not translate <name>"
msgid "Remove the applet named <name>"
msgstr "<name> ਐਪਲਿਟ ਹਟਾਓ"

#: amarokpkg.cpp:120
#, kde-format
msgid ""
"Absolute path to the package root. If not supplied, then the standard data "
"directories for this KDE session will be searched instead."
msgstr ""
"ਪੈਕੇਜ ਰੂਟ ਲਈ ਅਸਲੀ ਪਾਥ ਹੈ। ਜੇ ਇਹ ਨਾ ਦਿੱਤਾ ਤਾਂ ਇਸ KDE ਸ਼ੈਸ਼ਨ ਲਈ ਸਟੈਂਡਰਡ ਡਾਟਾ ਡਾਇਰੈਕਟਰੀਆਂ ਦੀ "
"ਖੋਜ ਕੀਤੀ ਜਾਵੇਗੀ।"

#: amarokpkg.cpp:176
#, kde-format
msgid "Successfully removed %1"
msgstr "%1 ਠੀਕ ਤਰ੍ਹਾਂ ਹਟਾਇਆ ਗਿਆ"

#: amarokpkg.cpp:178
#, kde-format
msgid "Removal of %1 failed."
msgstr "%1 ਨੂੰ ਹਟਾਉਣ ਲਈ ਫੇਲ੍ਹ"

#: amarokpkg.cpp:183
#, kde-format
msgid "Plugin %1 is not installed."
msgstr "ਪਲੱਗਇਨ %1 ਇੰਸਟਾਲ ਨਹੀਂ ਹੈ।"

#: amarokpkg.cpp:188
#, kde-format
msgid "Successfully installed %1"
msgstr "%1 ਠੀਕ ਤਰ੍ਹਾਂ ਇੰਸਟਾਲ ਕੀਤਾ"

#: amarokpkg.cpp:191
#, kde-format
msgid "Installation of %1 failed."
msgstr "%1 ਦੀ ਇੰਸਟਾਲੇਸ਼ਨ ਫੇਲ੍ਹ ਹੋਈ।"

#: amarokpkg.cpp:199
#, kde-format
msgctxt ""
"No option was given, this is the error message telling the user he needs at "
"least one, do not translate install, remove, upgrade nor list"
msgid "One of install, remove, upgrade or list is required."
msgstr "ਇੰਸਟਾਲ ਕਰਨ, ਹਟਾਉਣ, ਅੱਪਡੇਟ ਜਾਂ ਲਿਸਟ ਵੇਖਣ ਵਿੱਚੋਂ ਇੱਕ ਚਾਹੀਦਾ ਹੈ।"

#~ msgid "Install, list, remove Amarok applets"
#~ msgstr "ਅਮਰੋਕ ਐਪਲਿਟ ਇੰਸਟਾਲ ਕਰੋ, ਵੇਖੋ, ਹਟਾਓ"

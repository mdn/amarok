# translation of amarokpkg.po to Khmer
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Khoem Sokhem <khoemsokhem@khmeros.info>, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: amarokpkg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-04 01:45+0000\n"
"PO-Revision-Date: 2010-05-19 08:41+0700\n"
"Last-Translator: Khoem Sokhem <khoemsokhem@khmeros.info>\n"
"Language-Team: Khmer <support@khmeros.info>\n"
"Language: km\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: KBabel 1.11.4\n"
"X-Language: km_KH\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ខឹម សុខែម, ម៉ន ម៉េត, សេង សុត្ថា, អេង វណ្ណៈ, អោក ពិសិដ្ឋ"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"khoemsokhem@khmeros.info,​​mornmet@khmeros.info,sutha@khmeros.info, \n"
"evannak@khmeros.info,piseth_dv@khmeros.info"

#: amarokpkg.cpp:87 amarokpkg.cpp:99
#, kde-format
msgid "Amarok Applet Manager"
msgstr "កម្មវិធី​គ្រប់​គ្រង​អាប់ភ្លេត​របស់​ Amarok"

#: amarokpkg.cpp:89
#, kde-format
msgid "(C) 2008, Aaron Seigo, (C) 2009, Leo Franchi"
msgstr "រក្សាសិទ្ធិ​ឆ្នាំ ២​០​០​៨ ដោយ Aaron Seigo រក្សាសិទ្ធិ​ឆ្នាំ ២​០​០​៩ ដោយ Leo Franchi"

#: amarokpkg.cpp:90
#, kde-format
msgid "Aaron Seigo"
msgstr "Aaron Seigo"

#: amarokpkg.cpp:91
#, kde-format
msgid "Original author"
msgstr "អ្នក​និពន្ធ​ដើម"

#: amarokpkg.cpp:93
#, kde-format
msgid "Leo Franchi"
msgstr "Leo Franchi"

#: amarokpkg.cpp:94
#, kde-format
msgid "Developer"
msgstr "អ្នក​អភិវឌ្ឍន៍"

#: amarokpkg.cpp:110
#, kde-format
msgid "For install or remove, operates on applets installed for all users."
msgstr "ចំពោះ​ការ​ដំឡើង ឬ​យកចេញ ប្រតិបត្តិ​លើ​អាប់ភ្លេត​ដែល​បាន​ដំឡើង​សម្រាប់​អ្នកប្រើ​ទាំង​អស់ ។"

#: amarokpkg.cpp:112
#, kde-format
msgctxt "Do not translate <path>"
msgid "Install the applet at <path>"
msgstr "ដំឡើង​អាប់ភ្លេត​នៅ <path>"

#: amarokpkg.cpp:114
#, kde-format
msgctxt "Do not translate <path>"
msgid "Upgrade the applet at <path>"
msgstr "ធ្វើ​ឲ្យ​អាប់ភ្លេត​ប្រសើរ​ឡើង​នៅ <path>"

#: amarokpkg.cpp:116
#, fuzzy, kde-format
#| msgid "List installed applets"
msgid "Most installed applets"
msgstr "រាយអាប់ភ្លេត​ដែល​បាន​ដំឡើង"

#: amarokpkg.cpp:118
#, kde-format
msgctxt "Do not translate <name>"
msgid "Remove the applet named <name>"
msgstr "យក​អាប់ភ្លេត​ដែល​មាន​ឈ្មោះ <name>"

#: amarokpkg.cpp:120
#, kde-format
msgid ""
"Absolute path to the package root. If not supplied, then the standard data "
"directories for this KDE session will be searched instead."
msgstr ""
"ផ្លូវ​ពេញលេញ​ទៅកាន់​កញ្ចប់ root ។ ប្រសិន​បើ​មិន​បាន​ផ្ដល់ នោះ​ថត​ទិន្នន័យ​ស្តង់ដារ​សម្រាប់​សម័យ  KDE នឹង​ត្រូវ​"
"បាន​ស្វែង​រក​ជំនួស ។"

#: amarokpkg.cpp:176
#, kde-format
msgid "Successfully removed %1"
msgstr "បាន​យក %1 ចេញ​ដោយ​ជោគជ័យ"

#: amarokpkg.cpp:178
#, kde-format
msgid "Removal of %1 failed."
msgstr "បាន​បរាជ័យ​ក្នុង​ការ​យក​​ %1 ចេញ ។"

#: amarokpkg.cpp:183
#, kde-format
msgid "Plugin %1 is not installed."
msgstr "កម្មវិធី​ជំនួយ %1 មិន​ត្រូវ​បាន​ដំឡើង​ទេ ។"

#: amarokpkg.cpp:188
#, kde-format
msgid "Successfully installed %1"
msgstr "បាន​ដំឡើង %1 ដោយ​ជោគជ័យ"

#: amarokpkg.cpp:191
#, kde-format
msgid "Installation of %1 failed."
msgstr "បាន​បរាជ័យ​ក្នុង​កា​រ​ដំឡើង %1 ។"

#: amarokpkg.cpp:199
#, kde-format
msgctxt ""
"No option was given, this is the error message telling the user he needs at "
"least one, do not translate install, remove, upgrade nor list"
msgid "One of install, remove, upgrade or list is required."
msgstr "ត្រូវការ​ ការ​ដំឡើង យកចេញ ធ្វើ​ឲ្យ​ប្រសើរ​ឡើង ឬ​រាយ​មួយ ។"

#~ msgid "Install, list, remove Amarok applets"
#~ msgstr "ដំឡើង រាយ យក​អាប់ភ្លេត​ Amarok ចេញ"
